⍝⍝⍝ ngn/apl Jan 1 2021
⍝⍝⍝ // TRIPLE NESTED LOOP

⍝ I have to use apl for this one because practically
⍝ anything I write in apl will have nested loops in
⍝ the background.

chars←'.∘○⍟⍬⌹'
boxes←2
slct←{⊃⍵⌽⍺}

⍝ char box arr
box ← {⍺⍪(⍺,⍵,⍺)⍪⍺}

⍝ chars loop nesting
loop ← {
  ⍵<1: 1 1⍴⊃⍺
  (⊃⍺)box ⍪⍨,⍨(1⌽⍺)loop(⍵-1)
}

TNL ← chars loop 3

⍝https://n9n.gitlab.io/apl/web/index.html#code=%u235D%u235D%u235D%20ngn/apl%20Jan%201%202021%0A%u235D%u235D%u235D%20//%20TRIPLE%20NESTED%20LOOP%0A%0A%u235D%20I%20have%20to%20use%20apl%20for%20this%20one%20because%20practically%0A%u235D%20anything%20I%20write%20in%20apl%20will%20have%20nested%20loops%20in%0A%u235D%20the%20background.%0A%0Achars%u2190%27.%u2218%u25CB%u235F%u236C%u2339%27%0Aboxes%u21902%0Aslct%u2190%7B%u2283%u2375%u233D%u237A%7D%0A%0A%u235D%20char%20box%20arr%0Abox%20%u2190%20%7B%u237A%u236A%28%u237A%2C%u2375%2C%u237A%29%u236A%u237A%7D%0A%0A%u235D%20chars%20loop%20nesting%0Aloop%20%u2190%20%7B%0A%20%20%u2375%3C1%3A%201%201%u2374%u2283%u237A%0A%20%20%28%u2283%u237A%29box%20%u236A%u2368%2C%u2368%281%u233D%u237A%29loop%28%u2375-1%29%0A%7D%0A%0ATNL%20%u2190%20chars%20loop%203%0A