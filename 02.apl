⍝⍝⍝ ngn/apl genuary2021 Jan 2 2021
⍝⍝⍝ Rule 30

⍝ nextstate ← rules elemcellaut initialstate
elemcellaut ← {
  vec ← 0,⍵,0
  ⍺∊⍨{vec[⍵]}¨(⊂⍳3)+⍳≢⍵
}

r30 ← ((1 0 0)(0 1 1)(0 1 0)(0 0 1)) ∘ elemcellaut

rows ← {
  ⍺<1: ⍬
     ⋄ (⊂⍵) , (⍺-1) rows (r30 ⍵)
}

draw←{' ⌹'[↑⍵ rows ((⍵⍴0),1,(⍵⍴0))]}

draw 40

⍝ https://n9n.gitlab.io/apl/web/index.html#code=%u235D%u235D%u235D%20ngn/apl%20genuary2021%20Jan%202%202021%0A%u235D%u235D%u235D%20Rule%2030%0A%0A%u235D%20nextstate%20%u2190%20rules%20elemcellaut%20initialstate%0Aelemcellaut%20%u2190%20%7B%0A%20%20vec%20%u2190%200%2C%u2375%2C0%0A%20%20%u237A%u220A%u2368%7Bvec%5B%u2375%5D%7D%A8%28%u2282%u23733%29+%u2373%u2262%u2375%0A%7D%0A%0Ar30%20%u2190%20%28%281%200%200%29%280%201%201%29%280%201%200%29%280%200%201%29%29%20%u2218%20elemcellaut%0A%0Arows%20%u2190%20%7B%0A%20%20%u237A%3C1%3A%20%u236C%0A%20%20%20%20%20%u22C4%20%28%u2282%u2375%29%20%2C%20%28%u237A-1%29%20rows%20%28r30%20%u2375%29%0A%7D%0A%0Adraw%u2190%7B%27%20%u2339%27%5B%u2191%u2375%20rows%20%28%28%u2375%u23740%29%2C1%2C%28%u2375%u23740%29%29%5D%7D%0A%0Adraw%2040